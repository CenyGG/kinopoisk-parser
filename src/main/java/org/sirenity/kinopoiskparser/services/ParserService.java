package org.sirenity.kinopoiskparser.services;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.nodes.TextNode;
import org.sirenity.kinopoiskparser.db.Film;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class ParserService {
    private static final String TOP_250_ = "top250_place_";

    public List<Film> parse() throws Exception {
        Document doc = Jsoup.connect("https://www.kinopoisk.ru/top/").get();
        ArrayList<Film> filmsList = new ArrayList<Film>();

        for (int i = 1; i < 251; i++) {
            Element filmDivElement = doc.getElementById(TOP_250_ + i);
            filmsList.add(createFilmEntity(filmDivElement, i));
        }

        return filmsList;
    }

    private Film createFilmEntity(Element filmDivElement, int position) {
        List<Node> childNodes = filmDivElement.childNodes();
        String nameAndYear = ((TextNode) childNodes.get(3).childNode(0).childNode(0)).text();
        String name = nameAndYear.substring(0, nameAndYear.indexOf("(") - 1);
        int year = Integer.parseInt(nameAndYear.substring(nameAndYear.indexOf("(") + 1, nameAndYear.indexOf(")")));

        List<Node> ratingAndVotesNodes = childNodes.get(5).childNode(1).childNodes();

        Node ratingNode = ratingAndVotesNodes.get(1).childNodes().get(0);
        String ratingStr;
        String votesStr;
        if (ratingNode instanceof TextNode) {
            ratingStr = ((TextNode) ratingNode).text();
            votesStr = ((TextNode) ratingAndVotesNodes.get(3).childNode(0)).text();
        } else {
            ratingStr = ((TextNode) ratingNode.childNode(0)).text();
            votesStr = ((TextNode) ratingAndVotesNodes.get(1).childNode(2).childNode(0)).text();
        }

        float rating = Float.parseFloat(ratingStr);
        int votes = Integer.parseInt(votesStr.replaceAll("\\W", ""));

        return new Film(position, rating, name, year, votes, new Date());
    }
}
