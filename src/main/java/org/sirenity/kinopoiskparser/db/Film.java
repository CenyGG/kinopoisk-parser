package org.sirenity.kinopoiskparser.db;

import lombok.*;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Getter
@Setter
@NoArgsConstructor
public class Film {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private int position;
    private float rating;
    private String name;
    private int year;
    private int votes;
    @Temporal(TemporalType.DATE)
    private Date date;

    public Film(int position, float rating, String name, int year, int votes, Date date) {
        this.position = position;
        this.rating = rating;
        this.name = name;
        this.year = year;
        this.votes = votes;
        this.date = date;
    }
}
