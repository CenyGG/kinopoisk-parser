package org.sirenity.kinopoiskparser;

import org.sirenity.kinopoiskparser.db.Film;
import org.sirenity.kinopoiskparser.db.FilmRepository;
import org.sirenity.kinopoiskparser.services.ParserService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import java.util.List;

@SpringBootApplication
public class KinopoiskParserApplication {

	public static void main(String[] args) throws Exception {
		ConfigurableApplicationContext run = SpringApplication.run(KinopoiskParserApplication.class, args);

		List<Film> films = run.getBean(ParserService.class).parse();
		FilmRepository filmRepository = run.getBean(FilmRepository.class);
		films.forEach(filmRepository::save);
	}
}
